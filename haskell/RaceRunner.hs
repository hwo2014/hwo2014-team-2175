{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}

module RaceRunner(
  raceAutomaton
  ) where

import RaceAI

import System.IO (Handle)
import Text.Printf

import qualified Server
import qualified Model.Car as Car
import Model.CarPosition
import Model.GameInitData

data ConnectionState ai = CSWaitingYourCar
                        | CSWaitingGameInit Car.Id
                        | CSWaitingGameStart ai
                        | CSRace ai Tick
                        | CSWaitingNewGameInit ai
  deriving (Eq, Show)

connectionStateName :: ConnectionState a -> String
connectionStateName CSWaitingYourCar {}      = "'waiting your car'"
connectionStateName CSWaitingGameInit {}     = "'waiting game init'"
connectionStateName CSWaitingGameStart {}    = "'waiting game start'"
connectionStateName (CSRace _ tick)          = "'race " ++ show tick ++ "'"
connectionStateName CSWaitingNewGameInit {}  = "'waiting for new game init'"

raceAutomaton :: RaceAI ai => Handle -> (Car.Id -> GameInitData -> ai) -> IO ()
raceAutomaton h maker = loop CSWaitingYourCar
  where
    loop state =
      let unexpectedMsg msg = do
            putStrLn $ "Unexpected message in " ++ connectionStateName state
            print msg
            continue
          continue      = loop state 
      in do
       --putStrLn $ "state: " ++ connectionStateName state
       case state of
        CSWaitingYourCar -> do
          msg <- Server.receiveMessage h
          case msg of
            Server.YourCar identifier   -> loop $ CSWaitingGameInit identifier
            Server.JoinEcho             -> continue
            _                           -> unexpectedMsg msg

        CSWaitingGameInit identifier -> do
          msg <- Server.receiveMessage h
          case msg of
            Server.GameInit initial     -> loop $ CSWaitingGameStart (maker identifier initial)
            _                           -> unexpectedMsg msg

        CSWaitingNewGameInit ai -> do
          msg <- Server.receiveMessage h
          case msg of
            Server.GameInit _initial    -> loop $ CSWaitingGameStart ai -- TODO: tell ai about new data!
            Server.TournamentEnd        -> putStrLn "TOURNAMENT ENDED"
            _                           -> unexpectedMsg msg

        CSWaitingGameStart ai -> do
          msg <- Server.receiveMessage h
          case msg of
            Server.GameStart            -> do
              Server.sendMessage h $ Server.Throttle 1 -- Täysii
              loop $ CSRace ai 0
            _                           -> unexpectedMsg msg

        -- Game loop!
        CSRace ai prevTick -> do
          msg <- Server.receiveMessage h
          case msg of
            Server.GameEnd                   -> do
              Server.sendMessage h Server.Ping
              loop $ CSWaitingNewGameInit ai

            Server.CarPositions newTick pos  -> do
              validateTick prevTick newTick
              (newAI, clientMsg) <- advance ai newTick pos
              Server.sendMessage h clientMsg
              loop $ CSRace newAI newTick

            Server.Crash newTick i           -> do
              validateTick prevTick newTick
              newAI <- update ai $ Crash i
              loop $ CSRace newAI prevTick

            Server.Spawn newTick i           -> do
              validateTick prevTick newTick
              newAI <- update ai $ Spawn i
              loop $ CSRace newAI prevTick

            Server.Turbo duration factor     -> do
              newAI <- update ai $ Turbo duration factor
              loop $ CSRace newAI prevTick

            _                                -> unexpectedMsg msg

validateTick :: Tick -> Tick -> IO ()
validateTick prevTick newTick
  | newTick >= prevTick  = return ()
  | otherwise            = putStrLn $ printf "Ticks don't match! (%d -> %d)" prevTick newTick

