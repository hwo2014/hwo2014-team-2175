{-# LANGUAGE TemplateHaskell #-}

module ThrottleAI (ThrottleAI, mkThrottleAI) where

import qualified Server
import Model.Car as Car
import Model.GameInitData
import Model.CarPosition

import RaceAI (RaceAI, Tick, Info(..))
import qualified RaceAI

import StatefulAI

newtype ThrottleAI = ThrottleAI { unThrottleAI :: StatefulAI Float }

throttleMap :: (StatefulAI Float -> StatefulAI Float) -> ThrottleAI -> ThrottleAI
throttleMap f = ThrottleAI . f . unThrottleAI

mkThrottleAI :: Float -> Car.Id -> GameInitData -> ThrottleAI
mkThrottleAI throttle identifier gameInitData = ThrottleAI $ mkStatefulAI throttle identifier gameInitData

instance RaceAI ThrottleAI where
  advance = throttleAdvance
  update = throttleUpdate

throttleUpdate :: ThrottleAI -> RaceAI.Info -> IO ThrottleAI
throttleUpdate ai (Crash i) = return ai'
  where ai' = throttleMap (onCrash i) ai
throttleUpdate ai (Spawn i) = return ai'
  where ai' = throttleMap (onSpawn i) ai
throttleUpdate ai (Turbo dur fac) = return ai'
  where ai' = throttleMap (onTurbo dur fac) ai


throttleAdvance :: ThrottleAI -> Tick -> GameState -> IO (ThrottleAI, Server.ClientMessage)
throttleAdvance ai tick pos = return (ai', Server.Throttle . saiState . unThrottleAI $ ai)
  where ai' = throttleMap (onNewPositions tick pos) ai
