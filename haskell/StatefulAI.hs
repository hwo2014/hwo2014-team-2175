module StatefulAI where

import Control.Applicative

import qualified Model.Car as Car
import Model.GameInitData
import Model.CarPosition
import qualified Model.Track as Track

import RaceAI (Tick)

import Physics

data TurboState = NoTurbo
                | HaveTurbo Int Float -- duration, factor
                | TurboInUse Int Float
  deriving Show


data StatefulAI s = StatefulAI {
  saiGid           :: GameInitData,
  saiCar           :: String,
  saiGameStates    :: [GameState],
  saiObservations  :: [Observation],
  saiState         :: s,
  saiTurbo         :: TurboState
  }

observation :: GameInitData -> CarPosition -> CarPosition -> CarPosition -> Observation
observation gid prev curr next = Observation vel acc ang angD1 angDD invRadius
  where ang   = angle curr
        angD1 = angle curr - angle prev
        angD2 = angle next - angle curr
        angDD = angD2 - angD1
        vel   = positionDistance gid (piecePosition curr) (piecePosition next)
        velP  = positionDistance gid (piecePosition prev) (piecePosition curr)
        acc   = vel - velP
        currPiecePosition  = piecePosition curr
        currPiece          = piecesOfGame gid !! pieceIndex currPiecePosition :: Track.Piece
        currLane           = lanesOfGame gid !! startLaneIndex (pieceLane currPiecePosition) :: Track.Lane
        invRadius          = Track.signedLaneInvRadius currLane currPiece

mkStatefulAI :: s -> Car.Id -> GameInitData -> StatefulAI s
mkStatefulAI s identifier gameInitData =
  StatefulAI gameInitData (Car.idName identifier) [] [] s NoTurbo

onNewPositions :: Tick -> GameState -> StatefulAI s -> StatefulAI s
onNewPositions _tick pos ai = ai'
  where ai'              = ai { saiGameStates = newGameStates, saiObservations = newObservations, saiTurbo = newTurbo }
        newGameStates    = pos : saiGameStates ai
        car              = saiCar ai
        gid              = saiGid ai
        turbo            = saiTurbo ai
        newObservations  = case newObservation of
                             Just o   -> o : saiObservations ai
                             Nothing  -> saiObservations ai
        newObservation   = case newGameStates of
                             next : curr : prev : _ -> observation gid <$> prev' <*> curr' <*> next'
                                                         where prev' = findCar car prev
                                                               curr' = findCar car curr
                                                               next' = findCar car next
                             _                      -> Nothing
        newTurbo          = case turbo of
                              TurboInUse 0 _ -> NoTurbo
                              TurboInUse n f -> TurboInUse (n-1) f
                              _              -> turbo

onCrash :: Car.Id -> StatefulAI s -> StatefulAI s
onCrash _ = id

onSpawn :: Car.Id -> StatefulAI s -> StatefulAI s
onSpawn _ = id

onTurbo :: Int -> Float -> StatefulAI s -> StatefulAI s
onTurbo duration factor ai@StatefulAI { saiTurbo = turbo } =
  case turbo of
    NoTurbo         -> ai { saiTurbo = HaveTurbo duration factor }
    HaveTurbo _ _   -> ai { saiTurbo = HaveTurbo duration factor }
    TurboInUse _ _  -> ai

putState :: s -> StatefulAI s -> StatefulAI s
putState s ai = ai { saiState = s }

saiMaxThrottle :: StatefulAI s -> Float
saiMaxThrottle StatefulAI { saiTurbo = turbo} = 
  case turbo of
    TurboInUse _ factor  -> max 1.0 factor
    _                    -> 1.0

saiHaveTurbo :: StatefulAI s -> Bool
saiHaveTurbo StatefulAI { saiTurbo = turbo} = 
  case turbo of
    HaveTurbo _ _  -> True
    _              -> False

saiUseTurbo :: StatefulAI s -> StatefulAI s
saiUseTurbo ai@StatefulAI { saiTurbo = turbo } =
  case turbo of
    NoTurbo         -> ai
    HaveTurbo d f   -> ai { saiTurbo = TurboInUse d f  }
    TurboInUse _ _  -> ai
