{-# LANGUAGE OverloadedStrings #-}

module Model.Track(
  Lane(..), Piece(..), StartingPoint(..), Position(..), Track(..),
  laneRadius, laneDistance, switchableLanes, signedLaneInvRadius, pieceMaxVelocities, distanceTillBend, routeMaxVelocities, longestStraight
  ) where

import Data.Aeson ((.:), (.:?), Value(..), FromJSON(..))
import Control.Applicative ((<$>), (<*>), empty)
import Data.List (maximumBy)
import Data.Function (on)

-- Lane

data Lane = Lane {
  distanceFromCenter :: Float,
  index              :: Int
} deriving (Show, Eq)

instance FromJSON Lane where
  parseJSON (Object v) =
    Lane <$>
    (v .: "distanceFromCenter") <*>
    (v .: "index")
  parseJSON _          = empty
-- Piece

data Piece = StraightPiece { pieceLength :: Float, pieceSwitch :: Bool }
           | BendPiece { pieceRadius :: Float, pieceAngle :: Float, pieceSwitch :: Bool }
  deriving (Show, Eq)

instance FromJSON Piece where
  parseJSON (Object v) = do
    s <- maybe False id <$> v .:? "switch"
    r <- v .:? "radius"
    case r of
      Just r' -> do
        a <- v .: "angle"
        return $ BendPiece  r' a s
      Nothing -> do
        l <- v .: "length"
        return $ StraightPiece l s
  parseJSON _          = empty
-- StartingPoint

data StartingPoint = StartingPoint {
  startPosition :: Position,
  startAngle    :: Float
} deriving (Show, Eq)

instance FromJSON StartingPoint where
  parseJSON (Object v) =
    StartingPoint <$>
    (v .: "position") <*>
    (v .: "angle")
  parseJSON _          = empty
-- Position

data Position = Position {
  x :: Float,
  y :: Float
} deriving (Show, Eq)

instance FromJSON Position where
  parseJSON (Object v) =
    Position <$>
    (v .: "x") <*>
    (v .: "y")
  parseJSON _          = empty
-- Track

data Track = Track {
  trackName     :: String,
  startingPoint :: StartingPoint,
  pieces        :: [Piece],
  lanes         :: [Lane]
} deriving (Show, Eq)

instance FromJSON Track where
  parseJSON (Object v) =
    Track <$>
    (v .: "name") <*>
    (v .: "startingPoint") <*>
    (v .: "pieces") <*>
    (v .: "lanes")
  parseJSON _          = empty
-- Helpers

laneRadius :: Lane -> Piece -> Float
laneRadius _ StraightPiece {} = 1 / 0 -- Infinity
laneRadius (Lane offset _) (BendPiece r angle _)
  | angle < 0 = r + offset
  | otherwise = r - offset

signedLaneInvRadius :: Lane -> Piece -> Float
signedLaneInvRadius _ StraightPiece {} = 0
signedLaneInvRadius (Lane offset _) (BendPiece r angle _)
  | angle < 0 = 1.0 / (signum angle * (r + offset))
  | otherwise = 1.0 / (signum angle * (r - offset))

laneDistance :: Lane -> Piece -> Float
laneDistance _    (StraightPiece l _)  = l
laneDistance lane piece@(BendPiece _ a _) =
  (abs a / 360) * 2 * pi * laneRadius lane piece

switchableLanes :: [Lane] -> Lane -> [Lane]
switchableLanes allLanes currentLane =
  filter validSwitch allLanes
  where
    candidates = (+) <$> [index currentLane] <*> [-1, 0, 1]
    validSwitch l = index l `elem` candidates

nextBendPiece :: Int -> [Piece] -> (Float, Float, Bool, Int)
nextBendPiece idx ps = case ps !! (idx `mod` length ps) of
                             StraightPiece _ _   -> nextBendPiece (idx + 1) ps
                             BendPiece r a s     -> (r, a, s, idx `mod` length ps)

distanceTillBend :: Int -> [Piece] -> Float
distanceTillBend = distanceTillBend' 0
  where distanceTillBend' acc idx ps  = case ps !! (idx `mod` length ps) of
                                          BendPiece _ _ _    -> acc
                                          StraightPiece l _  -> distanceTillBend' (acc + l) (idx + 1) ps

longestStraight :: [Piece] -> Int
longestStraight ps = maximumBy (compare `on` f) [0..length ps - 1]
  where f idx = distanceTillBend idx ps

-- v = sqrt $ abs $ ((kd + kb maxangle) r / kc)
pieceMaxVelocities :: Float -> Float -> Float -> Float -> [Piece] -> [Float]
pieceMaxVelocities maxangle kb kc kd ps = map f [0 .. length ps]
  where
    f i = let (r, _, _, _) = nextBendPiece i ps
           in sqrt $ abs $ (kd + kb * maxangle) * r / kc

routeMaxVelocities :: Float -> Float -> Float -> Float -> [(Piece, Lane)] -> [Float]
routeMaxVelocities maxangle kb kc kd route = map f [0 .. length ps]
  where
    ps = map fst route
    f i = let (r, a, s, idx) = nextBendPiece i ps
              piece = BendPiece r a s
              lane = map snd route !! idx
              r' = laneRadius lane piece
           in sqrt $ abs $ (kd + kb * maxangle) * r' / kc
