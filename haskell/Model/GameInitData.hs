{-# LANGUAGE OverloadedStrings #-}

module Model.GameInitData(
  GameInitData,
  players, piecesOfGame, lanesOfGame, laneOfGame, reportGameInit, race, track
  ) where

import qualified Model.Car as Car
import qualified Model.Track as Track

import Data.List(find)
import Data.Maybe(fromJust)
import Data.Aeson ((.:), (.:?), Value(..), FromJSON(..))
import Control.Applicative ((<$>), (<*>), empty)

-- RaceSession

data RaceSession = RaceSession {
  laps :: Maybe Int,
  durationMs :: Maybe Int,
  maxLapTimeMs :: Maybe Int,
  quickRace :: Maybe Bool
  } deriving (Show)

instance FromJSON RaceSession where
  parseJSON (Object v) =
    RaceSession <$>
    (v .:? "laps") <*>
    (v .:? "durationMs") <*>
    (v .:? "maxLapTimeMs") <*>
    (v .:? "quickRace")
  parseJSON _          = empty

-- Race

data Race = Race {
  track :: Track.Track,
  cars  :: [Car.Car],
  raceSession :: RaceSession
} deriving (Show)

instance FromJSON Race where
  parseJSON (Object v) =
    Race <$>
    (v .: "track") <*>
    (v .: "cars") <*>
    (v .: "raceSession")
  parseJSON _          = empty

-- GameInitData

data GameInitData = GameInitData {
  race :: Race
} deriving (Show)

instance FromJSON GameInitData where
  parseJSON (Object v) =
    GameInitData <$>
    (v .: "race")
  parseJSON _          = empty

-- Helpers

players :: GameInitData -> [Car.Id]
players gameInit =
  map Car.id $ cars $ race gameInit

piecesOfGame :: GameInitData -> [Track.Piece]
piecesOfGame gameInit =
  Track.pieces $ track $ race gameInit

lanesOfGame :: GameInitData -> [Track.Lane]
lanesOfGame gameInit =
  Track.lanes $ track $ race gameInit

-- Lanes are maybe not in order?
laneOfGame :: GameInitData -> Int -> Track.Lane
laneOfGame gameInit index =
  fromJust $ find ((index ==) . Track.index) (lanesOfGame gameInit)

reportGameInit :: GameInitData -> String
reportGameInit gameInit =
  "Players: " ++ show (players gameInit) ++
  ", Track: " ++ show (length $ piecesOfGame gameInit) ++
  " pieces"

