{-# LANGUAGE OverloadedStrings #-}

module Model.CarPosition where

import Model.Car as Car
import Model.GameInitData
import Model.Track as Track

import Data.Aeson ((.:), FromJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>), empty)

import Data.List

-- CarLane

data CarLane = CarLane {
  startLaneIndex :: Int,
  endLaneIndex   :: Int
} deriving (Show)

instance FromJSON CarLane where
  parseJSON (Object v) =
    CarLane <$>
    (v .: "startLaneIndex") <*>
    (v .: "endLaneIndex")
  parseJSON _          = empty

-- PiecePosition

data PiecePosition = PiecePosition {
  pieceIndex      :: Int,
  inPieceDistance :: Float,
  pieceLane       :: CarLane,
  lap             :: Int
} deriving (Show)

instance FromJSON PiecePosition where
  parseJSON (Object v) =
    PiecePosition <$>
    (v .: "pieceIndex")      <*>
    (v .: "inPieceDistance") <*>
    (v .: "lane")            <*>
    (v .: "lap")
  parseJSON _          = empty

-- CarPosition

data CarPosition = CarPosition {
  carId         :: Car.Id,
  angle         :: Float,
  piecePosition :: PiecePosition
} deriving (Show)

instance FromJSON CarPosition where
  parseJSON (Object v) =
    CarPosition <$>
    (v .: "id") <*>
    (v .: "angle") <*>
    (v .: "piecePosition")
  parseJSON _          = empty

type GameState = [CarPosition]

-- Helpers

findCar :: String -> GameState -> Maybe CarPosition
findCar name =
  find nameMatches
  where nameMatches carPosition = Car.idName (carId carPosition) == name

leftInPiece :: GameInitData -> PiecePosition -> Float
leftInPiece gid (PiecePosition i distance lane _) = let
  getLane getter = laneOfGame gid $ getter lane
  laneDist lane' = Track.laneDistance lane' $ piecesOfGame gid !! i
  lane0 = getLane startLaneIndex
  lane1 = getLane endLaneIndex
  -- Use a average of the two lanes, not sure if this is how the server works
  in sum (map laneDist [lane0, lane1]) / 2 - distance

positionDistance :: GameInitData -> PiecePosition -> PiecePosition -> Float
positionDistance gid p0@(PiecePosition i0 d0 _ _) (PiecePosition i1 d1 _ _)
  | i0 == i1  = d1 - d0
  | otherwise = leftInPiece gid p0 + d1

carAdvance :: GameInitData -> String -> GameState -> GameState -> Maybe Float
carAdvance gid name ps0 ps1 = do
  p0 <- position ps0
  p1 <- position ps1
  return $ positionDistance gid p0 p1
  where position p = piecePosition <$> findCar name p
