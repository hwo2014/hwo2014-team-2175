{-# LANGUAGE OverloadedStrings #-}

module Model.Car where

import Data.Aeson ((.:), Value(..), FromJSON(..))
import Control.Applicative ((<$>), (<*>), empty)

-- CarId

data Id = Id {
  idColor  :: String,
  idName   :: String
} deriving (Show, Eq)

instance FromJSON Id where
  parseJSON (Object v) =
    Id <$>
    (v .: "color") <*>
    (v .: "name")
  parseJSON _          = empty
-- Dimension

data Dimensions = Dimensions {
  length            :: Float,
  width             :: Float,
  guideFlagPosition :: Float
} deriving (Show)

instance FromJSON Dimensions where
  parseJSON (Object v) =
    Dimensions <$>
    (v .: "length") <*>
    (v .: "width") <*>
    (v .: "guideFlagPosition")
  parseJSON _          = empty
-- Car

data Car = Car {
  id         :: Id,
  dimensions :: Dimensions
} deriving (Show)

instance FromJSON Car where
  parseJSON (Object v) =
    Car <$>
    (v .: "id") <*>
    (v .: "dimensions")
  parseJSON _          = empty
