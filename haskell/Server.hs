{-# LANGUAGE OverloadedStrings #-}

module Server(
  ClientMessage(..),
  ServerMessage(..),
  LaneDirection(..),
  connect, receiveMessage, sendMessage
  ) where

import Network(
  connectTo,
  PortID(..))
import System.IO(
  hPutStrLn, hGetLine, hSetBuffering,
  BufferMode(..), Handle)
import Control.Applicative

import qualified Data.ByteString.Lazy.Char8 as L

import Data.Aeson(
  decode, fromJSON, parseJSON, encode, object,
  FromJSON(..), ToJSON(..), Value(..), Result(..),
  (.:), (.:?), (.=))

import Model.GameInitData
import Model.CarPosition
import qualified Model.Car as Car

data LaneDirection = Left | Right deriving(Show)

data ClientMessage =
  Join String String | -- botname botkey
  JoinRaceMessage String String String Int String | -- botname botkey trackname carCount password
  Throttle Float |
  LaneSwitch LaneDirection |
  UseTurbo |
  Ping
  deriving (Show)

data ServerMessage =
  JoinEcho |
  YourCar Car.Id |
  GameStart |
  GameEnd |
  TournamentEnd |
  Finish |
  GameInit GameInitData |
  InitialCarPositions [CarPosition] |
  CarPositions Int [CarPosition] |
  Crash Int Car.Id |
  Spawn Int Car.Id |
  Turbo Int Float |
  Unknown String
  deriving (Show)

connect :: String -> Int -> IO Handle
connect server port = do
  h <- connectTo server (PortNumber $ fromIntegral port)
  hSetBuffering h LineBuffering
  return h

-- Message receiving

receiveMessage :: Handle -> IO ServerMessage
receiveMessage h = do
  msg <- hGetLine h
  --putStrLn $ "-> " ++ msg
  case decode (L.pack msg) of -- We should have instance FromJSON ServerMessage
    Just json -> return $ d $ fromJSON json
    Nothing   -> return $ Unknown msg
  where d (Success x) = x
        d (Error err) = Unknown err

data TurboData = TurboData Int Float

instance FromJSON TurboData where
  parseJSON (Object v) = TurboData <$> v .: "turboDurationTicks" <*> v .: "turboFactor"
  parseJSON _          = empty

instance FromJSON ServerMessage where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    case msgType of
      "join"           -> return JoinEcho
      "yourCar"        -> YourCar <$> carId
      "gameStart"      -> return GameStart
      "gameEnd"        -> return GameEnd
      "finish"         -> return Finish
      "tournamentEnd"  -> return TournamentEnd
      "turboAvailable" -> turbo <$> v .: "data"
      "crash"          -> Crash <$> tick <*> carId
      "spawn"          -> Spawn <$> tick <*> carId
      "gameInit"       -> GameInit <$> v .: "data"
      "carPositions"   -> do
        tick' <- v .:? "gameTick"
        case tick' of
          Just t   -> CarPositions t <$> carPos
          Nothing  -> InitialCarPositions <$> carPos
      _                -> return $ Unknown msgType
      where carId   = v .: "data"
            tick    = v .: "gameTick"
            carPos  = v .: "data"
            turbo (TurboData dur fac)  = Turbo dur fac
  parseJSON _          = empty

clientMessage :: String -> Value -> Value
clientMessage t d = object [ "msgType" .= t, "data" .= d]

botId :: String -> String -> Value
botId botName botKey = object [ "name" .= botName, "key" .= botKey ]

instance ToJSON ClientMessage where
  toJSON (JoinRaceMessage botName botKey trackName carCount password) =
    clientMessage "joinRace" $ object [
      "botId" .= botId botName botKey,
      "trackName" .= trackName,
      "carCount" .= carCount,
      "password" .= password]
  toJSON (Join botName botKey) =
    clientMessage "join" $ botId botName botKey
  toJSON Ping =
    clientMessage "ping" Null
  toJSON (LaneSwitch dir) =
    clientMessage "switchLane" $ toJSON $ show dir
  toJSON UseTurbo =
    clientMessage "turbo" $ toJSON ("wow" :: String)
  toJSON (Throttle amount) = 
    clientMessage "throttle" $ toJSON amount

sendMessage :: Handle -> ClientMessage -> IO ()
sendMessage h msg = do
  --putStrLn $ "<- " ++ show msg
  L.hPutStrLn h $ encode msg
