import sys
import re
import json
import math
import numpy
from pylab import *
from scipy import *
from scipy import optimize

def pieceLength(piece, lane):
	if "radius" in piece:
		return (piece["radius"] - lane["distanceFromCenter"]) * math.pi / 180.0 * abs(piece["angle"])
	else:
		return piece["length"]

def safediv(x, y):
	if y == "Infinity":
		return 0
	elif y == 0.0:
		return "Infinity"
	else:
		return x / y

if len(sys.argv) != 3:
	print "Usage: python", sys.argv[0], "log.file", "throttleK"
	sys.exit(1)

throttleK = float(sys.argv[2])

with file(sys.argv[1]) as f:
	lines = f.read().split("\n")
	lines = map(lambda x: x.strip(), lines)
	lines = filter(lambda x: x != "", lines)

	gameInit = lines[0]
	rest = lines[1:]

	pieces = json.loads(gameInit[3:])["data"]["race"]["track"]["pieces"]
	lanes = json.loads(gameInit[3:])["data"]["race"]["track"]["lanes"]

	print "Pieces", pieces
	print "Lanes", lanes

	positionsthr = []

	# Merge positions and throttle
	for i in xrange(len(rest) // 2):
		pos = json.loads(rest[i*2][3:])["data"][0]
		thr = float(rest[i*2 + 1][12:])		

		positionsthr.append((pos, thr))

	# Find k1 and kf
	x = 0

	xs = []
	dxs = []
	dvs = []
	angles = []
	dangles = []
	ddangles = []
	radiuses = []
	
	for i in xrange(2, len(positionsthr) - 1):
		pos = positionsthr[i][0]
		prevPos = positionsthr[i-1][0]
		nextPos = positionsthr[i+1][0]

		pieceIndex = pos["piecePosition"]["pieceIndex"]
		prevPieceIndex = prevPos["piecePosition"]["pieceIndex"]
		nextPieceIndex = nextPos["piecePosition"]["pieceIndex"]

		piece = pieces[pieceIndex]
		prevPiece = pieces[prevPieceIndex]
		nextPiece = pieces[nextPieceIndex]
		
		lane = lanes[pos["piecePosition"]["lane"]["startLaneIndex"]]
		prevLane = lanes[prevPos["piecePosition"]["lane"]["startLaneIndex"]]
		nextLange = lanes[nextPos["piecePosition"]["lane"]["startLaneIndex"]]

		if "radius" in piece:
			sign = 1 if piece["angle"] > 0 else -1
			radius = sign * (piece["radius"] - lane["distanceFromCenter"])
		else:
			radius = "Infinity"

#		if radius != "Infinity":
#			break

		if prevPieceIndex == pieceIndex:
			prevDx = pos["piecePosition"]["inPieceDistance"] - prevPos["piecePosition"]["inPieceDistance"]
		else:
			prevDx = pieceLength(prevPiece, prevLane) - prevPos["piecePosition"]["inPieceDistance"] + pos["piecePosition"]["inPieceDistance"]

		if nextPieceIndex == pieceIndex:
			nextDx = nextPos["piecePosition"]["inPieceDistance"] - pos["piecePosition"]["inPieceDistance"]
		else:
			nextDx = pieceLength(piece, lane) - pos["piecePosition"]["inPieceDistance"] + nextPos["piecePosition"]["inPieceDistance"]

		x = x + prevDx

		dv = nextDx - prevDx

		angle = pos["angle"]
		prevAngle = prevPos["angle"]
		nextAngle = nextPos["angle"]

		dangle = angle - prevAngle
		nextAngle = nextAngle - angle

		ddangle = nextAngle - dangle

		if abs(dv) > 3:
			print "big dv"
			print prevDx, nextDx
			print pieceIndex, pos["piecePosition"]
			print prevPieceIndex, prevPos["piecePosition"], prevPiece, pieceLength(prevPiece, prevLane)
			continue

		if abs(prevDx) < 0.1:
			print "small dx"
			print prevDx, prevDx, dv
			continue

		print thr, radius, x, prevDx, dv, angle, prevAngle, dangle

		xs.append(x)
		dxs.append(nextDx)
		dvs.append(dv)
		angles.append(angle)
		dangles.append(dangle)
		ddangles.append(ddangle)
		radiuses.append(radius)
		# print pos

	xs = xs[2:]
	dxs = dxs[2:]
	dvs = dvs[2:]
	angles = angles[2:]
	dangles = dangles[2:]
	ddangles = ddangles[2:]
	radiuses = radiuses[2:]

	k1, kf = polyfit(dxs, dvs, 1)

	print "k1 =", k1
	print "kf =", kf / throttleK

	# Slipping on the straight line

	print "k1kf lin:", linalg.lstsq(zip(dxs, [throttleK] * len(dxs)), numpy.array(dvs))

	f = []
	t = []
	for i in xrange(len(xs)):
		if len(f) == len(t):
			if abs(angles[i]) > 0.0 and radiuses[i] == "Infinity":
				f.append(i)
		else:
			if radiuses[i] != "Infinity":
				t.append(i)

	ranges = zip(f, t)
	longestrange = max(ranges, key=lambda r: r[1] - r[0])

	f, t = longestrange

	def damping(xdata, *params):
		#print "x", xdata
		#print "-exp", numpy.exp(-params[1] * xdata)
		return params[0] * numpy.exp(-params[1] * xdata) * numpy.cos(params[2] * xdata + params[3])

	dampingX = np.array(range(t-f))
	p0 = [angles[f], 1/float(t-f), math.pi/(t-f), 0.0]
	p1 = optimize.curve_fit(damping, dampingX, np.array(angles[f:t]), p0)[0]

	print p0
	print p1

	omegaD = p1[2]
	zetaomega0 = p1[1]

	print "zeta omega_0 =", zetaomega0
	print "omega_d =", omegaD

	linres = linalg.lstsq(zip(dangles, angles)[f:t], ddangles[f:t])
	print "LINRES:", linres

	a = linres[0][0]
	b = linres[0][1]

	matt = []
	ddangleX = []
	for i in xrange(len(xs)):
		if radiuses[i] == "Infinity" and ddangles[i] > 0.0:
			matt.append([dangles[i], angles[i]])
			ddangleX.append(ddangles[i])

	print "VARJOLINRESL:", linalg.lstsq(matt, ddangleX	)

	print "straight slip coefficients: a, b = ", a, b

	omega0 = math.sqrt(-b)
	zeta = -a / omega0 / 2.0

	print "lin: zeta omage_0", zeta * omega0
	print "lin: omega_d = ", omega0 * math.sqrt(1 - zeta**2)
	print "lin:", linres

	#print damping(dampingX, *p1)

	#plot(dampingX, angles[f:t], "bx-")
	#plot(dampingX, damping(dampingX, *p0), "g-")
	#plot(dampingX, damping(dampingX, *p1), "r--")
	#show()

	# Uncomment to see they are linear!
	# plot(dxs, dvs, "bx-")
	# show()

	offset = 350

	movement = numpy.array(xs)
	velocity = numpy.array(dxs)
	alphadd  = numpy.array(ddangles)
	alphadot = numpy.array(dangles)
	alpha    = numpy.array(angles)

	invrads  = numpy.array(map(lambda r: safediv(1, float(r)), radiuses))

	residual = alphadd + a * alphadot + b * alpha
	N = len(residual)

	vr = velocity ** 2 * invrads

	mat = zip(alphadot, alpha, vr)

	linres2 = linalg.lstsq(mat[offset:], alphadd[offset:])
	print linres
	print linres2

	#a = linres2[0][0]
	#b = linres2[0][1]
	#c = linres2[0][2]

	mat2 = []
	ys   = []
	started = 0
	for row, y in zip(mat[offset:], alphadd[offset:]):
		if row[1] > 0.0 and abs(row[2]) > 0.0:
			started += 1

		if started and row[1] <= 0.0 and abs(row[2]) < 0.0001:
			break

		if started:
			mat2.append(row)
			ys.append(y)

	#print "MAT2", mat2
	#print "Y", ys

	linres3 = linalg.lstsq(mat2, ys)
	print linres3

	mat3 = []
	ys = []
	for row, y in zip(mat, alphadd):
		ad = row[0]
		an = row[1]
		f = row[2]

		yy = y - a * ad - b * an

		if f < 0:
			yy = -yy
			f = -f

		if yy < 0.1:
			continue

		mat3.append(f)
		ys.append(yy)

	yymean = (max(ys) - min(ys)) * 0.8 + min(ys)
	mat3mean = (max(mat3) - min(mat3)) * 0.8 + min(mat3)

	mat4 = []
	y4 = []
	for row, y in zip(mat3, ys):
		if y > yymean:
			continue

		if row > mat3mean:
			continue

		mat4.append([row, 1])
		y4.append(y)

	print mat4, y4

	linres4 = linalg.lstsq(mat4, y4)
	print "threshold", yymean, mat3mean
	print linres4

	kc = linres4[0][0]
	kd  = linres4[0][1]

	print "abcd:", a, b, kc, kd

	subplot(121)
	plot(mat3, ys, "bx-")
	plot(map(lambda x: x[0], mat4), y4, "gx");
	plot(mat3, map(lambda x: kc * x + kd, mat3), 'r-')
	title("kc kd fit")


	#plot(range(N), alphadd, "b-")
	#plot(range(N), alphadot/max(alphadot), "c-")
	#plot(range(N), invrads/max(invrads), "r-")
	#show()

	#plot(range(N), residual	, "g-")
	#plot(range(N), invrads/max(invrads), "r-")
	#plot(range(N), c * vr, "b-")
	#show()

	modelalpha = [alpha[offset]]
	modeldotalpha = alphadot[offset]
	for i in xrange(offset+1, N):
		oldmodelalpha = modelalpha[-1] 
		if abs(vr[i]) > 0.0:
			cd = kc * vr[i] + kd
		else:
			cd = 0
		modelalphadd = a * modeldotalpha + b * oldmodelalpha + cd

		newmodelalpha = oldmodelalpha + modeldotalpha
		modeldotalpha += modelalphadd

		modelalpha.append(newmodelalpha)

	subplot(122)
	plot(range(offset,N), 10*invrads[offset:]/max(invrads), "r-")
	plot(range(offset,N), alpha[offset:], "b-")
	plot(range(offset,N), modelalpha, "g-")
	title("drift angle simulation")
	show()
