module Router where

import Data.List
import Data.Function(on)

import Model.Track

data PieceTransition = PieceTransition (Piece, Lane) [PieceTransition]

rotate :: Int -> [a] -> [a]
rotate i xs = (drop i xs) ++ (take i xs)

possibleLanes :: Piece -> Lane -> [Lane] -> [Lane]
possibleLanes piece prevLane allLanes
  | pieceSwitch piece = switchableLanes allLanes prevLane
  | otherwise = [prevLane]

transitions :: [Piece] -> Lane -> [Lane] -> PieceTransition
transitions []     _        _     = undefined
transitions (p:[]) prevLane _     = PieceTransition (p, prevLane) []
transitions (p:ps) prevLane allLanes =
  let transitions' lane = transitions ps lane allLanes
  in PieceTransition (p, prevLane) $ map transitions' (possibleLanes p prevLane allLanes)

transitionsForTrackPos :: Track -> Int -> Int -> PieceTransition
transitionsForTrackPos track pieceIndex laneIndex = let
  -- TODO how much should we include here to get a reliable result?
  -- there needs to be some overlap...
  allLanes = lanes track
  currentLane = allLanes !! laneIndex
  ps = rotate pieceIndex (pieces track)
  in transitions ps currentLane allLanes

type RoutePiece = (Piece, Lane)
type Route = [RoutePiece]

routes :: PieceTransition -> [Route]
routes (PieceTransition p []) = [[p]]
routes (PieceTransition p ts) = [p:ps | t <- ts, ps <- routes t]

routeLength :: Route -> Float
routeLength r = baseLength + switchPenalty
  where
    baseLength = foldl' (+) 0 $ map (uncurry $ flip laneDistance) r
    switchPenalty = fromIntegral (countSwitches r) * 0.1 -- arbitrary penalty

countSwitches :: Route -> Int
countSwitches rs = sum $ zipWith laneDiff rs (rotate 1 rs)
  where
    laneDiff a b = abs $ (laneIndex a) - (laneIndex b)
    laneIndex (_, lane) = index lane

shortestRouteForTrackPos :: Track -> Int -> Int -> Route
shortestRouteForTrackPos track pieceIndex laneIndex = let
  rs = routes $ transitionsForTrackPos track pieceIndex laneIndex
  in minimumBy (compare `on` routeLength) rs

shortestRouteForTrack :: Track -> Route
shortestRouteForTrack track = let
  rs = map (shortestRouteForTrackPos track 0) (map index $ lanes track)
  in minimumBy (compare `on` routeLength) rs
