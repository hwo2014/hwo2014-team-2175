module RoutingAI (RoutingAI, mkRoutingAI) where

import Data.Maybe (isNothing, fromJust, fromMaybe)
import Debug.Trace
import Text.Printf

import qualified Server
import qualified Router
import Model.Car as Car
import Model.Track
import Model.GameInitData
import Model.CarPosition

import RaceAI (RaceAI, Tick, Info(..))
import qualified RaceAI

import StatefulAI

data RoutingState = RoutingState {
  rsThrottle :: Float,
  rsSwitchingToLane :: Maybe Int
}

newtype RoutingAI = RoutingAI { unRoutingAI :: StatefulAI RoutingState }

routingMap :: (StatefulAI RoutingState -> StatefulAI RoutingState) -> RoutingAI -> RoutingAI
routingMap f = RoutingAI . f . unRoutingAI

mkRoutingAI :: Float -> Car.Id -> GameInitData -> RoutingAI
mkRoutingAI throttle identifier gameInitData = RoutingAI $ mkStatefulAI initialState identifier gameInitData
  where initialState = RoutingState throttle Nothing

instance RaceAI RoutingAI where
  advance = routingAdvance
  update = routingUpdate

routingUpdate :: RoutingAI -> RaceAI.Info -> IO RoutingAI
routingUpdate ai (Crash i) = return ai'
  where ai' = routingMap (onCrash i) ai
routingUpdate ai (Spawn i) = return ai'
  where ai' = routingMap (onSpawn i) ai
routingUpdate ai (Turbo dur fac) = return ai'
  where ai' = routingMap (onTurbo dur fac) ai

routingAdvance :: RoutingAI -> Tick -> GameState -> IO (RoutingAI, Server.ClientMessage)
routingAdvance ai tick gs = return (ai', msg)
  where ai'              = routingMap (putState state' . onNewPositions tick gs) ai 
        state            = saiState $ unRoutingAI ai
        (state', msg)    = selectRoute ai' state gs

selectRoute :: RoutingAI -> RoutingState -> GameState -> (RoutingState, Server.ClientMessage)
selectRoute ai rs gs
  | isNothing currentPos = (rs, defaultMsg)
  | otherwise            = selectLane rTrack rs (fromJust currentPos) defaultMsg
    where
      defaultMsg = Server.Throttle $ rsThrottle rs
      sai        = unRoutingAI ai
      currentPos = findCar (saiCar sai) gs
      rTrack     = track $ race $ saiGid sai
      

selectLane :: Track -> RoutingState -> CarPosition -> Server.ClientMessage -> (RoutingState, Server.ClientMessage)
selectLane rTrack rs carPos defaultMsg
  | Prelude.length route < 3           = (rs, defaultMsg)
  | nextLaneIndex < currentLogicalLane = (rs', Server.LaneSwitch Server.Left)
  | nextLaneIndex > currentLogicalLane = (rs', Server.LaneSwitch Server.Right)
  | otherwise                          = (rs, defaultMsg)
  where
    route = Router.shortestRouteForTrackPos rTrack (pieceIndex $ piecePosition carPos) currentLane
    currentLane = endLaneIndex $ pieceLane $ piecePosition carPos
    currentLogicalLane = fromMaybe currentLane (rsSwitchingToLane rs)
    rs' = rs { rsSwitchingToLane = Just nextLaneIndex }
    -- We need to look ahead two pieces since:
    --   The switch needs to happen before the switch piece
    --   The route will report the switched lane only after the switch
    (_, nextLane) = route !! 2
    nextLaneIndex = trace (printf "currentLane: %d, nextLane: %d" currentLane (index nextLane))
                    (index nextLane)
      
