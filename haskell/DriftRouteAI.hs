module DriftRouteAI (DriftRouteAI, mkDriftRouteAI) where

import Data.Maybe (listToMaybe, fromJust, isNothing, fromMaybe)
import Control.Applicative

import qualified Server
import qualified Model.Car as Car
import qualified Model.Track as Track
import Model.GameInitData
import Model.CarPosition

import RaceAI (RaceAI, Tick, Info(..))
import qualified RaceAI
import qualified Router

import StatefulAI
import Physics

gka, gkb, gkc, gkd :: Float
gka = (-0.099999853867)
gkb = (-0.00749989367605)
gkc = 3.32266374
gkd = (-0.87494043)

data DriftState = DZero
                | DVelocity (Float, Float) [Float] -- k1, kf, pieces max velocities
                | DHarmonic (Float, Float) (Float, Float) [Float]
                | DReal (Float, Float) (Float, Float) (Float, Float) [Float] -- (k1, kf) (ka, kb) (kc, kd) velocities
  deriving Show

data RoutingState = RoutingState {
  rsSwitchingToLane :: Maybe Int
}

data DriftRouteState = DriftRouteState {
  dState :: DriftState,
  rState :: RoutingState
}

newtype DriftRouteAI = DriftRouteAI { unDriftRouteAI :: StatefulAI DriftRouteState }

driftMap :: (StatefulAI DriftRouteState -> StatefulAI DriftRouteState) -> DriftRouteAI -> DriftRouteAI
driftMap f = DriftRouteAI . f . unDriftRouteAI

mkDriftRouteAI :: Car.Id -> GameInitData -> DriftRouteAI
mkDriftRouteAI identifier gameInitData = DriftRouteAI $ mkStatefulAI state identifier gameInitData
  where state = DriftRouteState DZero (RoutingState Nothing)

instance RaceAI DriftRouteAI where
  advance = driftAdvance
  update = driftUpdate

driftUpdate :: DriftRouteAI -> RaceAI.Info -> IO DriftRouteAI
driftUpdate ai (Crash i) = return ai'
  where ai' = driftMap (onCrash i) ai
driftUpdate ai (Spawn i) = return ai'
  where ai' = driftMap (onSpawn i) ai
driftUpdate ai (Turbo dur fac) = return ai'
  where ai' = driftMap (onTurbo dur fac) ai

initialThrottle :: Float
initialThrottle = 1.0

maxAngle :: Float
maxAngle = 65.0 -- TODO: should be around 40 when kb and kc are correct

driftAdvance :: DriftRouteAI -> Tick -> GameState -> IO (DriftRouteAI, Server.ClientMessage)
driftAdvance ai tick pos = do
  --print (throttle, state)
  --print lastObservation
  --print throttle
  --print $ saiTurbo $ unDriftRouteAI ai
  return result
  where ai'              = driftMap (putState newState . onNewPositions tick pos) ai
        result           = if currPieceIndex == turboIndex && throttle > 1.0 && saiHaveTurbo (unDriftRouteAI ai')
                              then (driftMap saiUseTurbo ai', Server.UseTurbo)
                              else (ai', rMsg)
        (rState', rMsg)  = selectRoute (clamp 0.0 1.0 $ throttle / maxThrottle) ai' (rState state) pos
        newState         = DriftRouteState newDState rState'
        maxThrottle      = saiMaxThrottle $ unDriftRouteAI ai'
        observations     = saiObservations $ unDriftRouteAI ai'
        lastObservation  = listToMaybe observations
        currVelocity     = maybe 0.0 id $ obVelocity <$> lastObservation
        state            = saiState $ unDriftRouteAI ai
        car              = saiCar $ unDriftRouteAI ai
        gid              = saiGid $ unDriftRouteAI ai
        carPosition      = fromJust $ findCar car pos 
        turboIndex       = Track.longestStraight pieces
        currPieceIndex   = pieceIndex $ piecePosition carPosition
        nextPieceIndex   = (currPieceIndex + 1) `mod` length pieces
        pieces           = piecesOfGame gid
        currPiece        = pieces !! currPieceIndex
        left             = leftInPiece gid $ piecePosition carPosition
        -- currLap          = lap $ piecePosition carPosition
        tillBend         = left + Track.distanceTillBend nextPieceIndex pieces
        bendVelocity k1 kf vs  = case pieces !! nextPieceIndex of
                                   Track.StraightPiece _ _  -> calculateThrottle k1 kf currVelocity $ currPieceVelocity
                                   Track.BendPiece _ _ _    -> calculateThrottle k1 kf currVelocity $ min currPieceVelocity nextPieceVelocity
                                   where currPieceVelocity  = vs !! currPieceIndex
                                         nextPieceVelocity  = vs !! nextPieceIndex
        throttle         = case dState state of
                             DZero                           -> initialThrottle
                             DVelocity (k1, kf) vs           -> case currPiece of
                                                                  Track.StraightPiece _ _ -> if currVelocity + k1 * tillBend < (vs !! currPieceIndex) - 1.0
                                                                                                then 100.0
                                                                                                else 0.0
                                                                  Track.BendPiece _ _ _   -> bendVelocity k1 kf vs + 0.1
                             DHarmonic (k1, kf) (ka, kb) vs  -> case currPiece of
                                                                  Track.StraightPiece _ _ -> if currVelocity + k1 * tillBend < (vs !! currPieceIndex) - 1.0
                                                                                                then 100.0
                                                                                                else 0.0
                                                                  Track.BendPiece _ _ _   -> bendVelocity k1 kf vs + 0.1
                             DReal (k1, kf) _ _ vs           -> case currPiece of
                                                                  Track.StraightPiece _ _ -> if currVelocity + k1 * tillBend < vs !! currPieceIndex
                                                                                                then 100.0
                                                                                                else 0.0
                                                                  Track.BendPiece _ _ _   -> bendVelocity k1 kf vs
        newDState        = case dState state of
                             DZero -> case k1kf observations initialThrottle of
                                        Just k1kf' -> DVelocity k1kf' velocities
                                          where
                                            route            = Router.shortestRouteForTrack $ track $ race gid
                                            --velocities       = Track.pieceMaxVelocities maxAngle gkb gkc gkd $ piecesOfGame gid
                                            velocities       = Track.routeMaxVelocities maxAngle gkb gkc gkd route
                                        Nothing       -> DZero
                             DVelocity (k1, kf) vs  -> case kakb observations of
                                                         Just kakb' -> DHarmonic (k1, kf) kakb' vs
                                                         Nothing           -> DVelocity (k1, kf) vs  
                             DHarmonic kakf' kakb' vs -> case kckd observations kakb' of
                                                            Just kckd'  -> DReal kakf' kakb' kckd' vs
                                                            Nothing     -> DHarmonic kakf' kakb' vs
                             s -> s


selectRoute :: Float -> DriftRouteAI -> RoutingState -> GameState -> (RoutingState, Server.ClientMessage)
selectRoute throttle ai rs gs
  | isNothing currentPos = (rs, defaultMsg)
  | otherwise            = selectLane rTrack rs (fromJust currentPos) defaultMsg
    where
      defaultMsg = Server.Throttle throttle
      sai        = unDriftRouteAI ai
      currentPos = findCar (saiCar sai) gs
      rTrack     = track $ race $ saiGid sai
      

selectLane :: Track.Track -> RoutingState -> CarPosition -> Server.ClientMessage -> (RoutingState, Server.ClientMessage)
selectLane rTrack rs carPos defaultMsg
  | Prelude.length route < 3           = (rs, defaultMsg)
  | nextLaneIndex < currentLogicalLane = (rs', Server.LaneSwitch Server.Left)
  | nextLaneIndex > currentLogicalLane = (rs', Server.LaneSwitch Server.Right)
  | otherwise                          = (rs, defaultMsg)
  where
    route = Router.shortestRouteForTrackPos rTrack (pieceIndex $ piecePosition carPos) currentLane
    currentLane = endLaneIndex $ pieceLane $ piecePosition carPos
    currentLogicalLane = fromMaybe currentLane (rsSwitchingToLane rs)
    rs' = rs { rsSwitchingToLane = Just nextLaneIndex }
    -- We need to look ahead two pieces since:
    --   The switch needs to happen before the switch piece
    --   The route will report the switched lane only after the switch
    (_, nextLane) = route !! 2
    nextLaneIndex = (Track.index nextLane)
      
