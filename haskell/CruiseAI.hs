module CruiseAI (CruiseAI, mkCruiseAI) where

import Data.Maybe (listToMaybe)
import Control.Applicative

import qualified Server
import qualified Model.Car as Car
import Model.GameInitData
import Model.CarPosition

import RaceAI (RaceAI, Tick, Info(..))
import qualified RaceAI

import StatefulAI
import Physics

data CruiseState = CruiseState {
  csVelocity :: Float,
  csCoefficients :: Maybe (Float, Float) -- k1, kf
}

newtype CruiseAI = CruiseAI { unCruiseAI :: StatefulAI CruiseState }

cruiseMap :: (StatefulAI CruiseState -> StatefulAI CruiseState) -> CruiseAI -> CruiseAI
cruiseMap f = CruiseAI . f . unCruiseAI

mkCruiseAI :: Float -> Car.Id -> GameInitData -> CruiseAI
mkCruiseAI velocity identifier gameInitData = CruiseAI $ mkStatefulAI initialState identifier gameInitData
  where initialState = CruiseState velocity Nothing

instance RaceAI CruiseAI where
  advance = cruiseAdvance
  update = cruiseUpdate

cruiseUpdate :: CruiseAI -> RaceAI.Info -> IO CruiseAI
cruiseUpdate ai (Crash i) = return ai'
  where ai' = cruiseMap (onCrash i) ai
cruiseUpdate ai (Spawn i) = return ai'
  where ai' = cruiseMap (onSpawn i) ai
cruiseUpdate ai (Turbo dur fac) = return ai'
  where ai' = cruiseMap (onTurbo dur fac) ai

initialThrottle :: Float
initialThrottle = 1.0

cruiseAdvance :: CruiseAI -> Tick -> GameState -> IO (CruiseAI, Server.ClientMessage)
cruiseAdvance ai tick pos = do
  print (throttle, coefficients, lastObservation)
  return (ai', Server.Throttle $ clamp 0.0 1.0 throttle)
  where ai'              = cruiseMap (putState newState . onNewPositions tick pos) ai 
        observations     = saiObservations $ unCruiseAI ai'
        state            = saiState $ unCruiseAI ai
        newState         = state { csCoefficients = coefficients}
        coefficients     = csCoefficients state <|> k1kf observations initialThrottle
        lastObservation  = listToMaybe observations
        targetVelocity   = csVelocity state
        throttle         = maybe initialThrottle id $ f <$> coefficients <*> lastObservation
        f (k1, kf) Observation { obVelocity = lastVelocity}
                         = calculateThrottle k1 kf lastVelocity targetVelocity
