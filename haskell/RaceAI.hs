module RaceAI where

import qualified Server
import qualified Model.Car as Car
import Model.CarPosition


type Tick = Int

data Info = Crash Car.Id
          | Spawn Car.Id
          | Turbo Int Float


class RaceAI a where
  advance :: a -> Tick -> GameState -> IO (a, Server.ClientMessage)
  update  :: a -> Info -> IO a
