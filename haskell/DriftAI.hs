module DriftAI (DriftAI, mkDriftAI) where

import Data.Maybe (listToMaybe, fromJust)
import Control.Applicative

import qualified Server
import qualified Model.Car as Car
import qualified Model.Track as Track
import Model.GameInitData
import Model.CarPosition

import RaceAI (RaceAI, Tick, Info(..))
import qualified RaceAI

import StatefulAI
import Physics

gka, gkb, gkc, gkd :: Float
gka = (-0.099999853867)
gkb = (-0.00749989367605)
gkc = 3.32266374
gkd = (-0.87494043)

data DriftState = DZero
                | DVelocity (Float, Float) [Float] -- k1, kf, pieces max velocities
                | DHarmonic (Float, Float) (Float, Float) [Float]
                | DReal (Float, Float) (Float, Float) (Float, Float) [Float] -- (k1, kf) (ka, kb) (kc, kd) velocities
  deriving Show

newtype DriftAI = DriftAI { unDriftAI :: StatefulAI DriftState }

driftMap :: (StatefulAI DriftState -> StatefulAI DriftState) -> DriftAI -> DriftAI
driftMap f = DriftAI . f . unDriftAI

mkDriftAI :: Car.Id -> GameInitData -> DriftAI
mkDriftAI identifier gameInitData = DriftAI $ mkStatefulAI DZero identifier gameInitData

instance RaceAI DriftAI where
  advance = driftAdvance
  update = driftUpdate

driftUpdate :: DriftAI -> RaceAI.Info -> IO DriftAI
driftUpdate ai (Crash i) = return ai'
  where ai' = driftMap (onCrash i) ai
driftUpdate ai (Spawn i) = return ai'
  where ai' = driftMap (onSpawn i) ai
driftUpdate ai (Turbo dur fac) = return ai'
  where ai' = driftMap (onTurbo dur fac) ai

initialThrottle :: Float
initialThrottle = 1.0

maxAngle :: Float
maxAngle = 65.0 -- TODO: should be around 40 when kb and kc are correct

driftAdvance :: DriftAI -> Tick -> GameState -> IO (DriftAI, Server.ClientMessage)
driftAdvance ai tick pos = do
  print (throttle, state)
  print lastObservation
  print throttle
  print $ saiTurbo $ unDriftAI ai
  return result
  where ai'              = driftMap (putState newState . onNewPositions tick pos) ai
        result           = if currPieceIndex == turboIndex && throttle > 1.0 && saiHaveTurbo (unDriftAI ai')
                              then (driftMap saiUseTurbo ai', Server.UseTurbo)
                              else (ai', Server.Throttle $ clamp 0.0 1.0 $ throttle / maxThrottle)
        maxThrottle      = saiMaxThrottle $ unDriftAI ai'
        observations     = saiObservations $ unDriftAI ai'
        lastObservation  = listToMaybe observations
        currVelocity     = maybe 0.0 id $ obVelocity <$> lastObservation
        state            = saiState $ unDriftAI ai
        car              = saiCar $ unDriftAI ai
        gid              = saiGid $ unDriftAI ai
        carPosition      = fromJust $ findCar car pos
        turboIndex       = Track.longestStraight pieces
        currPieceIndex   = pieceIndex $ piecePosition carPosition
        nextPieceIndex   = (currPieceIndex + 1) `mod` length pieces
        pieces           = piecesOfGame gid
        currPiece        = pieces !! currPieceIndex
        left             = leftInPiece gid $ piecePosition carPosition
        -- currLap          = lap $ piecePosition carPosition
        tillBend         = left + Track.distanceTillBend nextPieceIndex pieces
        bendVelocity k1 kf vs  = case pieces !! nextPieceIndex of
                                   Track.StraightPiece _ _  -> calculateThrottle k1 kf currVelocity $ currPieceVelocity
                                   Track.BendPiece _ _ _    -> calculateThrottle k1 kf currVelocity $ min currPieceVelocity nextPieceVelocity
                                   where currPieceVelocity  = vs !! currPieceIndex
                                         nextPieceVelocity  = vs !! nextPieceIndex
        throttle         = case state of
                             DZero                           -> initialThrottle
                             DVelocity (k1, kf) vs           -> case currPiece of
                                                                  Track.StraightPiece _ _ -> if currVelocity + k1 * tillBend < (vs !! currPieceIndex) - 1.0
                                                                                                then 100.0
                                                                                                else 0.0
                                                                  Track.BendPiece _ _ _   -> bendVelocity k1 kf vs + 0.1
                             DHarmonic (k1, kf) (ka, kb) vs  -> case currPiece of
                                                                  Track.StraightPiece _ _ -> if currVelocity + k1 * tillBend < (vs !! currPieceIndex) - 1.0
                                                                                                then 100.0
                                                                                                else 0.0
                                                                  Track.BendPiece _ _ _   -> bendVelocity k1 kf vs + 0.1
                             DReal (k1, kf) _ _ vs           -> case currPiece of
                                                                  Track.StraightPiece _ _ -> if currVelocity + k1 * tillBend < vs !! currPieceIndex
                                                                                                then 100.0
                                                                                                else 0.0
                                                                  Track.BendPiece _ _ _   -> bendVelocity k1 kf vs
        newState         = case state of
                             DZero -> case k1kf observations initialThrottle of
                                        Just k1kf' -> DVelocity k1kf' velocities
                                          where velocities       = Track.pieceMaxVelocities maxAngle gkb gkc gkd $ piecesOfGame gid
                                        Nothing       -> DZero
                             DVelocity (k1, kf) vs  -> case kakb observations of
                                                         Just kakb' -> DHarmonic (k1, kf) kakb' vs
                                                         Nothing           -> DVelocity (k1, kf) vs  
                             DHarmonic kakf' kakb' vs -> case kckd observations kakb' of
                                                            Just kckd'  -> DReal kakf' kakb' kckd' vs
                                                            Nothing     -> DHarmonic kakf' kakb' vs
                             s -> s
