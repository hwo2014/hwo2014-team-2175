module Physics where

import Numeric.Matrix (Matrix)
import qualified Numeric.Matrix as Matrix

import Data.Maybe

import Control.Applicative

import Debug.Trace

debug x = traceShow x x

data Observation = Observation {
    obVelocity       :: Float,
    obAcceleration   :: Float,
    obAngle          :: Float,
    obAngleD         :: Float,
    obAngleDD        :: Float,
    obSignInvRadius  :: Float
}
  deriving (Show)

{-
Velocity equation:

v' = k1 v + kf throttle

constant velocity:
v' = 0 => throttle = - k1 / kf targetVelocity
-}

nth :: Int -> [a] -> Maybe a
nth _ []      = Nothing
nth 0 (x:_)   = Just x
nth n (_:xs)  = nth (n-1) xs

-- least squares approximation of overdetermined system m b = y
-- (mT * m) b = mT * y => b = inv (mT * m) * mT * y
-- ltlsq m y, where m is List M (List N Float), y is List M Float, and result is Maybe (List N float)
ltlsq :: [[Float]] -> [Float] -> Maybe [Float]
ltlsq m' y' = beta''
  where m       = Matrix.fromList m'
        y       = Matrix.fromList $ map (:[]) y'
        mt      = Matrix.transpose m
        invmtm  = Matrix.inv (mt * m) :: Maybe (Matrix Float)
        beta    = Matrix.toList . (* (mt * y)) <$> invmtm :: Maybe [[Float]]
        beta'   = map listToMaybe <$> beta :: Maybe [Maybe Float]
        beta''  = beta' >>= sequence -- join (sequence <$> beta')

-- Calculate k1 and kf coeffecients, assuming constant throttle (second argument)
-- Uses least squares approximation
k1kf :: [Observation] -> Float -> Maybe (Float, Float)
k1kf os throttle
  | length os < 10  = Nothing
  | otherwise       = pair -- Just (-0.0204081632653, 0.204081632653) -- hardcoded
  where y       = map obAcceleration os
        m       = map (\o -> [obVelocity o, throttle]) os
        beta    = ltlsq m y
        pair    = do
          beta'  <- beta
          k1     <- nth 0 beta'
          kf     <- nth 1 beta'
          return (k1, kf)

kakb :: [Observation] -> Maybe (Float, Float)
kakb obs
  | length obs' < 20 = Nothing
  | otherwise        = result
  where obs'    = filter (\o -> abs (obAngle o) > 0.00 && obSignInvRadius o == 0.0) obs
        y       = map obAngleDD obs'
        m       = map (\o -> [obAngleD o, obAngle o]) obs'
        beta    = ltlsq m y
        result  = do
          result' <- beta
          ka      <- nth 0 result'
          kb      <- nth 1 result'
          return (ka, kb)

-- TODO: this doesn't work yet
kckd :: [Observation] -> (Float, Float) -> Maybe (Float, Float)
kckd obs (ka, kb)
  | length obs' < 20 = Nothing
  | otherwise        = result
  where obs'    = drop 10 $ filter (\o -> abs (obAngle o) > 0.0 && abs (obAngleDD o) > 0.01 && abs (obSignInvRadius o) > 0.0) obs
        obs''   = map fm obs'
        y       = map snd obs''
        m       = map (\x -> [fst x, 1.0]) obs''
        fm o    = if y' < 0 then (negate m', negate y') else (m', y')
          where m' = traceShow o $ obVelocity o * obVelocity o * obSignInvRadius o - ka * obAngleD o - kb * obAngle o
                y' = obAngleDD o
        beta    = traceShow (ka, kb, obs'') $ ltlsq m y
        result  = do
          result' <- beta
          kc      <- nth 0 result'
          kd      <- nth 1 result'
          return (kc, kd)

clamp :: Ord a => a -> a -> a -> a
clamp lo hi x
  | x < lo     = lo
  | x > hi     = hi
  | otherwise  = x

-- throttle = k1/kf * (v1 - v0 exp k1) / (1 - exp k1)
calculateThrottle :: Float -> Float -> Float -> Float -> Float
calculateThrottle k1 kf v0 v1 = k1 / kf * (v1 - v0 * expk1) / (expk1 - 1)
  where expk1 = exp k1
