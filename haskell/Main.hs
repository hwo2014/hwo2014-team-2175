module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import qualified Server
import RaceRunner (raceAutomaton)

-- AIs
import ThrottleAI (mkThrottleAI)
import CruiseAI (mkCruiseAI)
import DriftAI (mkDriftAI)
import DriftRouteAI (mkDriftRouteAI)

main :: IO ()
main = do
  args <- getArgs
  case args of
    (server : port : botName : botKey : rest) -> do
      h <- Server.connect server (read port)
      case rest of
        ["join", trackName, carCount, password] -> do
          putStrLn "Joining race!"
          Server.sendMessage h $ Server.JoinRaceMessage botName botKey trackName (read carCount :: Int) password
          raceAutomaton h mkDriftRouteAI
        (track : rest') | track `elem` ["keimola", "usa", "germany", "france"] -> do
          putStrLn $ "racing on " ++ track
          Server.sendMessage h $ Server.JoinRaceMessage botName botKey track 1 ""
          case rest' of
            ["throttle", throttle] -> raceAutomaton h $ mkThrottleAI $ read throttle
            ["cruise", velocity]   -> raceAutomaton h $ mkCruiseAI $ read velocity
            ["drift"]              -> raceAutomaton h mkDriftAI
            [throttle]             -> raceAutomaton h $ mkThrottleAI $ read throttle
            _                      -> raceAutomaton h mkDriftAI
        _ -> do
          putStrLn $ "racing on default track"
          Server.sendMessage h $ Server.Join botName botKey
          raceAutomaton h mkDriftRouteAI
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey> [otherstuff]"
      exitFailure
